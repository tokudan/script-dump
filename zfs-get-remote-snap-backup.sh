#!/run/current-system/sw/bin/bash -e

set -euo pipefail

PATH=$PATH:/run/current-system/sw/bin

# Local zfs data set to use as destination
LOCAL_BASE=data/backup/myserver/data
DATASETS=( srv/mail srv/nginx srv/postgresql srv/postgresqlBackup )

# login...
SSHKEY=$(dirname ${BASH_SOURCE})/myserver.id_rsa
SSHLOGIN=root@myserver


# Run the backup
SNAPSHOT_PREFIX=backup-
SNAPSHOT=${SNAPSHOT_PREFIX}$(date +%Y%m%dT%H%M%S)

# Setup the snapshots...
ssh -i ${SSHKEY} ${SSHLOGIN} zfs snapshot -r $(for ds in ${DATASETS[@]}; do echo ${ds}@${SNAPSHOT}; done)

# Now receive the zfs datasets.
for ds in ${DATASETS[@]}; do
	local_base_ds=${LOCAL_BASE}/$(basename $ds)
	echo Local base for $ds is $local_base_ds >&2
	# Check if a local dataset exists and identify the local snapshot
	if zfs list -H $local_base_ds >&2; then
		last_snap=$(zfs list -H -rt snapshot ${local_base_ds} | awk '{print $1}' | fgrep -- ${local_base_ds}@${SNAPSHOT_PREFIX} | tail -1 | awk -F@ '{print $2}')
		[ -n "${last_snap}" ] && incr="-i ${last_snap}"
	else
		echo Local base does not exist yet, creating a full stream >&2
		incr=''
	fi

	# Transfer the stream
	ssh -i ${SSHKEY} ${SSHLOGIN} zfs send --replicate --props ${incr} ${ds}@${SNAPSHOT} \| HOME=/ mbuffer -q -m 128m | \
		HOME=/ mbuffer -q -m 128m | \
		zfs receive -F -d -v ${LOCAL_BASE}

	# Remove old remote snapshots
	# Get the list of (sub-)datasets
	subds=$(ssh -i ${SSHKEY} ${SSHLOGIN} zfs list -H -rt snapshot ${ds} | awk -F@ '{print $1}' | egrep -- ^${ds} | sort -u)
	for sds in ${subds}; do
		ssh -i ${SSHKEY} ${SSHLOGIN} zfs list -H -rt snapshot ${sds} | awk '{print $1}' | fgrep -- ${sds}@${SNAPSHOT_PREFIX} | fgrep @ | head -n -28 | ssh -i ${SSHKEY} ${SSHLOGIN} xargs -t -n 1 -r zfs destroy 
	done
done
